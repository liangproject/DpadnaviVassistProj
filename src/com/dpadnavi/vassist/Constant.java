package com.dpadnavi.vassist;

public class Constant {
    public static final String EXTRA_KEY = "key";
    public static final String EXTRA_SECRET = "secret";
    public static final String EXTRA_SAMPLE = "sample";
    public static final String EXTRA_SOUND_START = "sound_start";
    public static final String EXTRA_SOUND_END = "sound_end";
    public static final String EXTRA_SOUND_SUCCESS = "sound_success";
    public static final String EXTRA_SOUND_ERROR = "sound_error";
    public static final String EXTRA_SOUND_CANCEL = "sound_cancel";
    public static final String EXTRA_INFILE = "infile";
    public static final String EXTRA_OUTFILE = "outfile";

    public static final String EXTRA_LANGUAGE = "language";
    public static final String EXTRA_NLU = "nlu";
    public static final String EXTRA_VAD = "vad";
    public static final String EXTRA_PROP = "prop";

    public static final String EXTRA_OFFLINE_ASR_BASE_FILE_PATH = "asr-base-file-path";
    public static final String EXTRA_LICENSE_FILE_PATH = "license-file-path";
    public static final String EXTRA_OFFLINE_LM_RES_FILE_PATH = "lm-res-file-path";
    public static final String EXTRA_OFFLINE_SLOT_DATA = "slot-data";
    public static final String EXTRA_OFFLINE_SLOT_NAME = "name";
    public static final String EXTRA_OFFLINE_SLOT_SONG = "song";
    public static final String EXTRA_OFFLINE_SLOT_ARTIST = "artist";
    public static final String EXTRA_OFFLINE_SLOT_APP = "app";
    public static final String EXTRA_OFFLINE_SLOT_USERCOMMAND = "usercommand";

    public static final int SAMPLE_8K = 8000;
    public static final int SAMPLE_16K = 16000;

    public static final String VAD_SEARCH = "search";
    public static final String VAD_INPUT = "input";
    
    // ============================ 语音引擎解析出的意图 start ============================
    public static final String VOICE_WEATHER = "weather";//天气
    
    public static final String VOICE_STOCK = "stock";//股票
    
    public static final String VOICE_CONTACTS = "contacts";//通讯录
    public static final String VOICE_TELEPHONE = "telephone";//打电话
    
    public static final String VOICE_MUSIC = "music";//音乐
    public static final String VOICE_PLAYER= "player";//音乐操作
    public static final String VOICE_INTENT_MUSIC_PLAY= "play";//播放音乐
    public static final String VOICE_INTENT_MUSIC_DOWNLOAD= "download";//下载音乐
    public static final String VOICE_INTENT_MUSIC_SEARCH= "search";//搜索音乐
    public static final String VOICE_INTENT_MUSIC_SET= "set";//音乐控制
    
    public static final String VOICE_RADIO= "radio";//收音机
    
    public static final String VOICE_NAVIGATE= "navigate";//导航
    // ============================ 语音引擎解析出的意图 end ============================
    
    // ============================ 语音引擎解析出的意图 start ============================
    public static final String AK = "8UI6PHtek99nSfQdnoC0Pawf";
    public static final String BAIDU_SERVICE = "http://api.map.baidu.com/telematics/v3/";
    public static final String SERVICE_WEATHER = "weather";//天气
    
    // ============================ 语音引擎解析出的意图 end ============================
}
