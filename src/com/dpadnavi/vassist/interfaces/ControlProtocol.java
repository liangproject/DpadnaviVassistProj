/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : ControlProtocol.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.interfaces
 * @Author : tomliang
 * @CreateDate : 2015-9-2
 */
package com.dpadnavi.vassist.interfaces;

/**
 * @Module : 控制协议
 * @Comments : 服务和后台共同的功能定义
 * @Author : tomliang
 * @CreateDate : 2015-9-2
 * @ModifiedBy : tomliang
 * @ModifiedDate: 2015-9-2
 * @Modified: 2015-9-2: 实现基本功能
 */
public interface ControlProtocol {
	/**
	 * 
	 * @author : tomliang
	 * @description : 天气操作
	 * @date : 2015-9-2 : void
	 */
	void todoWeather();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 电话操作
	 * @date : 2015-9-2 : void
	 */
	void todoCall();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 音乐操作
	 * @date : 2015-9-2 : void
	 */
	void todoMusic();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 导航操作
	 * @date : 2015-9-2 : void
	 */
	void todoGuide();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 股票操作
	 * @date : 2015-9-2 : void
	 */
	void todoStock();
}
