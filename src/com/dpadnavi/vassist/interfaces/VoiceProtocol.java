/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : VoiceProtocol.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.interfaces
 * @Author : tomliang
 * @CreateDate : 2015-9-3
 */
package com.dpadnavi.vassist.interfaces;

import android.content.Context;

/**
 * @Module : 语音协议
 * @Comments : 语音功能接口，解藕第三方语音和工程关系
 * @Author : tomliang
 * @CreateDate : 2015-9-3
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-3
 * @Modified: 2015-9-3: 实现基本功能
 */
public interface VoiceProtocol<Data> {
	
	/**
	 * 异常
	 */
	public static final int EVENT_ERROR = 0x000000;
	/**
	 * 引擎切换,例如在线转离线
	 */
	public static final int EVENT_ENGINE_SWITCH = 0x000001;

	/**
	 * 
	 * @author : tomliang
	 * @description : 语音sdk初始化
	 * @date : 2015-9-3
	 * @param context : void
	 */
	void init(Context context);
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 开始识别
	 * @date : 2015-9-3 : void
	 */
	void start();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 停止录音
	 * @date : 2015-9-3 : void
	 */
	void stop();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 取消识别
	 * @date : 2015-9-3 : void
	 */
	void cancle();
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 销毁
	 * @date : 2015-9-3 : void
	 */
	void destory();
	
	/**
	 * @author : tomliang
	 * @description : 设置声音解析回调
	 * @param listener
	 */
	void setVoiceListener(VoiceListener<Data> listener);
}
