package com.dpadnavi.vassist.interfaces;

public interface VoiceListener<Data> {

	/**
	 * 
	 * @author : tomliang
	 * @description : 声音引擎准备就绪后调用
	 * @date : 2015-9-3 : void
	 */
	void onReadyForSpeech();
	
	/**
	 * @author : tomliang
	 * @description : 返回临时结果时调用
	 * @param results
	 */
	void onPartialResults(String[] results);
	
	/**
	 * 
	 * @author : tomliang
	 * @description : 当声音引擎返回数据时调用
	 * @param data
	 */
	void onResults(Data data);
	
	/**
	 * @author : tomliang
	 * @description : 错误事件
	 * @param error
	 */
	void onError(int error);

	/**
	 * @author : tomliang
	 * @description : 当检测到用户的已经停止说话
	 * @date : 2015-9-3 : void
	 */
	void onEndOfSpeech();
	
}
