/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : BaseProtocol.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.http
 * @Author : tomliang
 * @CreateDate : 2015-9-7
 */
package com.dpadnavi.vassist.http.protocol;

import java.util.Map;

import com.dpadnavi.vassist.model.BaseModel;
import com.dpadnavi.vassist.view.ui.LoadingPage.LoadResult;
import com.loopj.android.http.RequestParams;

/**
 * @Module : 隶属模块名
 * @Comments : 描述
 * @Author : tomliang
 * @CreateDate : 2015-9-7
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-7
 * @Modified: 2015-9-7: 实现基本功能
 * @param <Data>
 */
public abstract class BaseProtocol<Data> {
	public Map<String, String> paramesMap;
    public BaseModel postParames;
    public RequestParams postParameMaps;
    public BaseProtocol(Map<String, String> paramesMap) {
        this.paramesMap = paramesMap;
    }

    public BaseProtocol(BaseModel postParames) {
        this.postParames = postParames;
    }
    
    public BaseProtocol(RequestParams postParameMaps) {
        this.postParameMaps = postParameMaps;
    }

    /**
	 * 检查服务器返回的json是成功或是失败
	 * networkUtils.access方式请求 json检查
	 * null对象
	 * @return
	 */
    public boolean checkJson(Data data){
        if(data instanceof BaseModel){
            if(((BaseModel)data).error == 0){
                return true;
            }
//            UIUtils.showToastSafe("status == 2");
        }
        return false;
    }
	
	/** 
	 * loadding 方式请求 json检查
	 * */
	public LoadResult check(Data data) {
		if (data == null)
			return LoadResult.ERROR;
		if(!checkJson(data))
			return LoadResult.EMPTY;
		return LoadResult.SUCCEED;
	}

	/** 该协议的访问地址 */
	public abstract String getKey();

	/** 从json中解析 */
    public abstract Data parseFromJson(String json);
}
