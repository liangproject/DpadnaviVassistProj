package com.dpadnavi.vassist.http.protocol;

import java.util.HashMap;

import android.util.Log;

import com.dpadnavi.vassist.Constant;
import com.dpadnavi.vassist.model.WeatherModel;
import com.dpadnavi.vassist.utils.GsonUtils;

/**
 * @author dg
 * @desc 天气的请求
 * @version 创建时间：2015-9-11
 */
public class WeatherProtocol extends BaseProtocol<WeatherModel> {

	public WeatherProtocol(HashMap<String, String> info) {
		super(info);
	}
	
	@Override
	public String getKey() {
		return Constant.BAIDU_SERVICE+"weather?";
	}

	@Override
	public WeatherModel parseFromJson(String json) {
		Log.i("WeatherProtocol", "WeatherProtocol >>> "+json);
		return GsonUtils.json2bean(json, WeatherModel.class);
	}

}
