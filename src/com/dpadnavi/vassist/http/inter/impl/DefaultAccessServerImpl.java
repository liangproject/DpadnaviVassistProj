package com.dpadnavi.vassist.http.inter.impl;

import com.dpadnavi.vassist.R;
import com.dpadnavi.vassist.http.inter.IAccessServer;
import com.dpadnavi.vassist.manager.ActivityManager;
import com.dpadnavi.vassist.model.BaseModel;
import com.dpadnavi.vassist.utils.StringUtils;
import com.dpadnavi.vassist.utils.UIUtils;
import com.dpadnavi.vassist.view.ui.CustomLoadingDialog;


public abstract class DefaultAccessServerImpl<Data> implements IAccessServer<Data> {

	@Override
	public void onPreExecute(String loadingMessage) {
		if(StringUtils.isEmpty(loadingMessage)){
			CustomLoadingDialog.getInstance(ActivityManager.getManager().getCurrentActivity()).show();
		}else{
			CustomLoadingDialog.getInstance(ActivityManager.getManager().getCurrentActivity()).showMessage(loadingMessage).show();
		}
	}
	
	@Override
	public void onPostExecute() {
		CustomLoadingDialog.getInstance(ActivityManager.getManager().getCurrentActivity()).dismiss();
	}

	
	@Override
	public void success(Data data) {
		showMessage(data, UIUtils.getString(R.string.connect_succ));
	}

	@Override
	public void failure(Data data) {
		showMessage(data, UIUtils.getString(R.string.connect_fail));
	}

	@Override
	public void networkErr() {
		//提示无法访问网络
		UIUtils.showToastSafe(UIUtils.getString(R.string.network_err));
	}

	/**
	 * 显示服务器返回message
	 * @param data
	 */
	private void showMessage(Data data, String msg){
		if(data instanceof BaseModel){
			UIUtils.showToastSafe(msg);
		}
	}

}
