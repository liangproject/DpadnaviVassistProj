package com.dpadnavi.vassist.http.inter;

public interface IAccessServer<Data> {
	
	/**
	 * 在执行网络请求前，执行
	 * 在UI线程 调用
	 */
	void onPreExecute(String loadingMessage);
	
	/**
	 * 网络请求完成后回调
	 * 在UI线程 调用
	 */
	void onPostExecute();
	
	/** 
	 * 请求服务器成功，并返回成功的jsonBean
	 * 在UI线程回调
	 * */
	void success(Data data);
	
	/** 
	 * 请求服务器成功，返回错误的jsonBean
	 * 在UI线程回调
	 * */
	void failure(Data data);
	
	/**
	 * 请求服务器失败，网络问题
	 * 在UI线程回调
	 * */
	void networkErr();
}
