/**
 * Copyright (c) 2012-2013 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : MessageSender.java
 * @ProjectName : VoiceCenter
 * @PakageName : cn.yunzhisheng.voicecenter.service
 * @Author : Brant
 * @CreateDate : 2013-7-5
 */
package com.dpadnavi.vassist.manager;

import com.dpadnavi.vassist.utils.LogUtils;
import com.dpadnavi.vassist.utils.UIUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

/**
 * @Module : 隶属模块名
 * @Comments : 描述
 * @Author : Brant
 * @CreateDate : 2013-7-5
 * @ModifiedBy : Brant
 * @ModifiedDate: 2013-7-5
 * @Modified: 2013-7-5: 实现基本功能
 */
public class MessageSender {
	private static final String TAG = "MessageSender";
	private static final MessageSender instance = new MessageSender();

	private MessageSender() {
	}
	
	public static MessageSender getInstance(){
		return instance;
	}

	public void sendMessage(Intent intent, String receiverPermission) {
		LogUtils.i(TAG, "sendMessage:intent " + intent + ",permission " + receiverPermission);
		Bundle bundle = intent.getExtras();
		if (bundle != null) {
			for (String key : bundle.keySet()) {
				LogUtils.i(TAG, "sendMessage:intent key " + key + ",value " + bundle.get(key));
			}
		}
		UIUtils.getContext().sendBroadcast(intent, receiverPermission);
	}

	public void sendMessage(Intent intent) {
		sendMessage(intent, null);
	}

	public void sendMessage(String message, Bundle extras, String receiverPermission) {
		Intent intent = new Intent(message);
		intent.putExtras(extras);
		sendMessage(intent, receiverPermission);
	}

	public void sendMessageWithBundle(String message, Bundle extras) {
		sendMessage(message, extras, null);
	}

	public void sendMessage(String action, String receiverPermission) {
		Intent intent = new Intent(action);
		sendMessage(intent, receiverPermission);
	}

	public void sendMessage(String action) {
		sendMessage(action, null);
	}

	public void sendOrderedMessage(String action) {
		sendOrderedMessage(action, null);
	}

	public void sendOrderedMessage(String action, String receiverPermission) {
		Intent intent = new Intent(action);
		sendOrderedMessage(intent, receiverPermission);
	}

	public void sendOrderedMessage(Intent intent, String receiverPermission) {
		LogUtils.i(TAG, "sendMessage:intent " + intent + ",permission " + receiverPermission);
		Bundle bundle = intent.getExtras();
		if (bundle != null) {
			for (String key : bundle.keySet()) {
				LogUtils.i(TAG, "sendMessage:intent key " + key + ",value " + bundle.get(key));
			}
		}
		UIUtils.getContext().sendOrderedBroadcast(intent, receiverPermission);
	}
}
