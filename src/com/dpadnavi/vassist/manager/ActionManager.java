/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : ActionManager.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.manager
 * @Author : tomliang
 * @CreateDate : 2015-9-7
 */
package com.dpadnavi.vassist.manager;

import java.util.List;

import com.dpadnavi.vassist.Constant;
import com.dpadnavi.vassist.model.BaiduVoiceResponeModel;
import com.dpadnavi.vassist.model.GetWeatherModel;
import com.dpadnavi.vassist.model.ResultsEntity;
import com.dpadnavi.vassist.utils.GsonUtils;
import com.dpadnavi.vassist.utils.UIUtils;


/**
 * @Module : 隶属模块名
 * @Comments : 语音解析后进行相应的匹配分发
 * @Author : tomliang
 * @CreateDate : 2015-9-7
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-7
 * @Modified: 2015-9-7: 实现基本功能
 */
public class ActionManager {

	public static ActionManager instance = new ActionManager();
	private ActionManager(){
		
	}
	public static ActionManager getInstance(){
		return instance;
	}
	/** 
	 * @author : tomliang
	 * @description : 匹配语音意图
	 * @date : 2015-9-7
	 * @param data : void 
	 */
	public void checkAction(BaiduVoiceResponeModel data) {
		if(data == null) return;
		if(data.getResults().size() == 0) return;
		
		ResultsEntity result = data.getResults().get(0);
		
		if(result.getDomain().equals(Constant.VOICE_WEATHER)){//天气识别
			GetWeatherModel weather = GsonUtils.json2bean(data.getRaw_json(), GetWeatherModel.class);
			String intent = weather.getResults().get(0).getIntent();
			String region = weather.getResults().get(0).getObject().getRegion();
			UIUtils.showToastSafe("去查询天气:"+intent+region);
		}else if(result.getDomain().equals(Constant.VOICE_MUSIC)){//音乐
			UIUtils.showToastSafe("去查询音乐");
			MusicControlManager.getInstance().paserIntent(data);
		}else if(result.getDomain().equals(Constant.VOICE_PLAYER)){//音乐播放
			UIUtils.showToastSafe("音乐操作");
			MusicControlManager.getInstance().paserIntent(data);
		}else if(result.getDomain().equals(Constant.VOICE_CONTACTS)){//通讯录
			UIUtils.showToastSafe("通讯录相关");
			
		}else if(result.getDomain().equals(Constant.VOICE_TELEPHONE)){//电话
			UIUtils.showToastSafe("电话相关");
			
		}else if(result.getDomain().equals(Constant.VOICE_NAVIGATE)){//导航
			UIUtils.showToastSafe("导航相关");
			
		}else if(result.getDomain().equals(Constant.VOICE_RADIO)){//收音机
			UIUtils.showToastSafe("收音机相关");
			
		}else if(result.getDomain().equals(Constant.VOICE_STOCK)){//股票
			UIUtils.showToastSafe("股票相关");
			
		}
	}
}
