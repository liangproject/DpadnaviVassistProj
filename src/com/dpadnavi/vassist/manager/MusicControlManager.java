/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : MusicManager.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.manager
 * @Author : tomliang
 * @CreateDate : 2015-9-9
 */
package com.dpadnavi.vassist.manager;

import java.util.List;
import android.content.Intent;
import android.widget.Toast;
import cn.kuwo.autosdk.api.KWAPI;
import cn.kuwo.autosdk.api.OnPlayEndListener;
import cn.kuwo.autosdk.api.OnSearchListener;
import cn.kuwo.autosdk.api.PlayEndType;
import cn.kuwo.autosdk.api.SearchStatus;
import cn.kuwo.autosdk.bean.Music;
import com.dpadnavi.vassist.Constant;
import com.dpadnavi.vassist.model.BaiduVoiceResponeModel;
import com.dpadnavi.vassist.model.DownLoadMusicModel;
import com.dpadnavi.vassist.model.PlayMusicModel;
import com.dpadnavi.vassist.model.ResultsEntity;
import com.dpadnavi.vassist.model.SearchMusicModel;
import com.dpadnavi.vassist.model.SongModel;
import com.dpadnavi.vassist.perfernce.CommandPreference;
import com.dpadnavi.vassist.utils.GsonUtils;
import com.dpadnavi.vassist.utils.UIUtils;

/**
 * @Module : 隶属模块名
 * @Comments : 描述
 * @Author : tomliang
 * @CreateDate : 2015-9-9
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-9
 * @Modified: 2015-9-9: 实现基本功能
 */
public class MusicControlManager
		implements
			BaseControlManager<BaiduVoiceResponeModel>,
			OnSearchListener {

	public static MusicControlManager instance = new MusicControlManager();
	private KWAPI mKwapi;
	private MessageSender mSender;

	private MusicControlManager() {
		mSender = MessageSender.getInstance();
		initKwApi();
	}

	public static MusicControlManager getInstance() {
		return instance;
	}

	@Override
	public void paserIntent(BaiduVoiceResponeModel model) {
		ResultsEntity entity = model.getResults().get(0);
		if (Constant.VOICE_INTENT_MUSIC_SEARCH.equals(entity.getIntent())) {
			SearchMusicModel search = GsonUtils.json2bean(model.getRaw_json(),
					SearchMusicModel.class);
			search(search);
		} else if (Constant.VOICE_INTENT_MUSIC_PLAY.equals(entity.getIntent())) {
			PlayMusicModel play = GsonUtils.json2bean(model.getRaw_json(),
					PlayMusicModel.class);
			play(play);
		} else if (Constant.VOICE_INTENT_MUSIC_DOWNLOAD.equals(entity
				.getIntent())) {
			DownLoadMusicModel download = GsonUtils.json2bean(
					model.getRaw_json(), DownLoadMusicModel.class);
			download(download);
		}
	}

	/**
	 * 
	 * @author : tomliang
	 * @description : 搜索歌曲
	 * @date : 2015-9-9
	 * @param model
	 *            : void
	 */
	public void search(SearchMusicModel model) {
		ResultsEntity resultsEntity = model.getResults().get(0);
		// StringBuilder builder = new StringBuilder();
		// 在线搜索
		SongModel object = resultsEntity.getObject();
		Intent intent = new Intent(CommandPreference.ACTION_MUSIC_DATA);
		intent.putExtra(SongModel.INTENT_SONGMODEL, object);
		mSender.sendOrderedMessage(intent, null);
	}

	public String getSafeStr(String str) {
		return str == null ? "" : str;
	}

	/**
	 * 
	 * @author : tomliang
	 * @description : 播放歌曲
	 * @date : 2015-9-9
	 * @param model
	 *            : void
	 */
	public void play(PlayMusicModel model) {
		ResultsEntity resultsEntity = model.getResults().get(0);
		// 在线搜索
		SongModel object = resultsEntity.getObject();
		Intent intent = new Intent(CommandPreference.ACTION_MUSIC_DATA);
		intent.putExtra(SongModel.INTENT_SONGMODEL, object);
		mSender.sendOrderedMessage(intent, null);
	}

	/**
	 * 
	 * @author : tomliang
	 * @description : 下载歌曲
	 * @date : 2015-9-9
	 * @param model
	 *            : void
	 */
	public void download(DownLoadMusicModel model) {
		com.dpadnavi.vassist.model.DownLoadMusicModel.ResultsEntity resultsEntity = model
				.getResults().get(0);
		UIUtils.showToastSafe("下载歌曲:" + resultsEntity.getObject().getName());
	}

	public void setMusic(ResultsEntity model) {

	}

	public void initKwApi() {
		// key值不要为空
		mKwapi = KWAPI.createKWAPI(UIUtils.getContext(), "auto");

		// 注册歌曲播放记录的监听，这个接口用于计算播歌数，一般不需要设置！！！
		mKwapi.registerPlayEndListener(UIUtils.getContext(),
				new OnPlayEndListener() {
					/**
					 * 用于计算歌曲的播放数，回调一次表示播放完一首歌曲
					 * 
					 * @param arg0
					 *            ， END_COMPLETE, //正常播放完 END_USER, //用户切歌
					 *            END_ERROR //异常结束
					 */
					@Override
					public void onPlayEnd(PlayEndType arg0) {
						// do something by yourself
						Toast.makeText(UIUtils.getContext(), arg0.toString(),
								Toast.LENGTH_LONG).show();
					}
				});

	}

	@Override
	public void searchFinshed(SearchStatus arg0, boolean arg1, List arg2,
			boolean arg3) {
		// 这里将List转化为List<Music>
		List<Music> musics = (List<Music>) arg2;
		if (arg0 == SearchStatus.SUCCESS) {
			// 搜索成功，可以将歌曲列表展现出来选择性的播放，这里测试默认播放第一首歌
			if (musics.size() > 0) {
				UIUtils.showToastSafe("即将播放歌曲：" + musics.get(0).name);
			}
		} else {
			// 搜索失败，自己进行相关提示处理
			UIUtils.showToastSafe("在线歌曲搜索失败！");
		}
	}

}
