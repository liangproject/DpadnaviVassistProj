/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : BaseControlManager.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.manager
 * @Author : tomliang
 * @CreateDate : 2015-9-9
 */
package com.dpadnavi.vassist.manager;

/**
 * @Module : 隶属模块名
 * @Comments : 描述
 * @Author : tomliang
 * @CreateDate : 2015-9-9
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-9
 * @Modified: 2015-9-9: 实现基本功能
 */
public interface BaseControlManager<Model> {

	/**
	 * 
	 * @author : tomliang
	 * @description : 解析意图
	 * @date : 2015-9-9
	 * @param model : void
	 */
	void paserIntent(Model model);
}
