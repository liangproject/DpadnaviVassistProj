/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : BaseModel.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.model
 * @Author : tomliang
 * @CreateDate : 2015-9-7
 */
package com.dpadnavi.vassist.model;

/**
 * @Module : 隶属模块名
 * @Comments : 描述
 * @Author : tomliang
 * @CreateDate : 2015-9-7
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-7
 * @Modified: 2015-9-7: 实现基本功能
 */
public class BaseModel {

	public int error;
	public String status;
	public String date;
}
