package com.dpadnavi.vassist.model;

import java.util.List;

/**
 * Created by Administrator on 2015/9/11.
 */
public class PlayMusicModel {
    /**
     * raw_text : 播放时间都去哪儿了
     * parsed_text : 播放 时间 都 去哪儿 了
     * results : [{"score":1,"domain":"music","update":1,"intent":"play","demand":0,"object":{"name":"时间都去哪儿","type":"歌曲"}}]
     */
    private String raw_text;
    private String parsed_text;
    private List<ResultsEntity> results;

    public void setRaw_text(String raw_text) {
        this.raw_text = raw_text;
    }

    public void setParsed_text(String parsed_text) {
        this.parsed_text = parsed_text;
    }

    public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }

    public String getRaw_text() {
        return raw_text;
    }

    public String getParsed_text() {
        return parsed_text;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }
}
