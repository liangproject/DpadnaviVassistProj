package com.dpadnavi.vassist.model;

import java.util.List;

/**
 * Created by Administrator on 2015/9/11.
 */
public class SearchMusicModel {

    /**
     * raw_text : 搜索最近热门的歌曲
     * parsed_text : 搜索 最近 热门 的 歌曲
     * results : [{"score":1,"domain":"music","update":1,"intent":"search","demand":0,"object":{"datepublish":"2014-01-01,2014-12-31","tag":["热门"],"type":"歌曲"}}]
     */
    private String raw_text;
    private String parsed_text;
    private List<ResultsEntity> results;

    public void setRaw_text(String raw_text) {
        this.raw_text = raw_text;
    }

    public void setParsed_text(String parsed_text) {
        this.parsed_text = parsed_text;
    }

    public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }

    public String getRaw_text() {
        return raw_text;
    }

    public String getParsed_text() {
        return parsed_text;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }

}
