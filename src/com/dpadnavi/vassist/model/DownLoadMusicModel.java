package com.dpadnavi.vassist.model;

import java.util.List;

/**
 * Created by Administrator on 2015/9/11.
 */
public class DownLoadMusicModel {
    /**
     * raw_text : 下载红玫瑰
     * parsed_text : 下载 红玫瑰
     * results : [{"score":1,"domain":"music","update":1,"intent":"download","demand":0,"object":{"name":"红玫瑰","type":"歌曲"}}]
     */
    private String raw_text;
    private String parsed_text;
    private List<ResultsEntity> results;

    public void setRaw_text(String raw_text) {
        this.raw_text = raw_text;
    }

    public void setParsed_text(String parsed_text) {
        this.parsed_text = parsed_text;
    }

    public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }

    public String getRaw_text() {
        return raw_text;
    }

    public String getParsed_text() {
        return parsed_text;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }

    public class ResultsEntity {
        /**
         * score : 1
         * domain : music
         * update : 1
         * intent : download
         * demand : 0
         * object : {"name":"红玫瑰","type":"歌曲"}
         */
        private int score;
        private String domain;
        private int update;
        private String intent;
        private int demand;
        private ObjectEntity object;

        public void setScore(int score) {
            this.score = score;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public void setUpdate(int update) {
            this.update = update;
        }

        public void setIntent(String intent) {
            this.intent = intent;
        }

        public void setDemand(int demand) {
            this.demand = demand;
        }

        public void setObject(ObjectEntity object) {
            this.object = object;
        }

        public int getScore() {
            return score;
        }

        public String getDomain() {
            return domain;
        }

        public int getUpdate() {
            return update;
        }

        public String getIntent() {
            return intent;
        }

        public int getDemand() {
            return demand;
        }

        public ObjectEntity getObject() {
            return object;
        }

        public class ObjectEntity {
            /**
             * name : 红玫瑰
             * type : 歌曲
             */
            private String name;
            private String type;

            public void setName(String name) {
                this.name = name;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getName() {
                return name;
            }

            public String getType() {
                return type;
            }
        }
    }
}
