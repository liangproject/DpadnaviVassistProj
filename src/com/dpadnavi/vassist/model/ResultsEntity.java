package com.dpadnavi.vassist.model;

public class ResultsEntity {
	private int score;
    private String domain;
    private int update;
    private String intent;
    private int demand;
    private SongModel object;
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public int getUpdate() {
		return update;
	}
	public void setUpdate(int update) {
		this.update = update;
	}
	public String getIntent() {
		return intent;
	}
	public void setIntent(String intent) {
		this.intent = intent;
	}
	public int getDemand() {
		return demand;
	}
	public void setDemand(int demand) {
		this.demand = demand;
	}
	public SongModel getObject() {
		return object;
	}
	public void setObject(SongModel object) {
		this.object = object;
	}
}
