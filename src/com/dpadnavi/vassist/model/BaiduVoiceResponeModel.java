package com.dpadnavi.vassist.model;

import java.util.List;

/**
 * Copyright 公司名 All right reserved.
 *
 * @FileName : TestBaiduVoiceResponeModel
 * @ProjectName : Android
 * @PakageName : cn.com.hui8.bean
 * @Author : tomliang
 * @CreateDate : 15/9/8
 */
public class BaiduVoiceResponeModel {

    private List<ResultsEntity> results;
    private String parsed_text;
    private String raw_text;
    private String raw_json;

    public String getRaw_json() {
		return raw_json;
	}

	public void setRaw_json(String raw_json) {
		this.raw_json = raw_json;
	}

	public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }

    public void setParsed_text(String parsed_text) {
        this.parsed_text = parsed_text;
    }

    public void setRaw_text(String raw_text) {
        this.raw_text = raw_text;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }

    public String getParsed_text() {
        return parsed_text;
    }

    public String getRaw_text() {
        return raw_text;
    }
}
