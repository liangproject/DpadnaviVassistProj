package com.dpadnavi.vassist.model;

import java.util.List;

/**
 * Created by Administrator on 2015/9/11.
 */
public class GetWeatherModel {
    /**
     * raw_text : 今天天气怎么样
     * parsed_text : 今天 天气 怎么样
     * results : [{"score":1,"domain":"weather","update":1,"intent":"get","demand":0,"object":{"date":"2015-09-10,2015-09-10","_date":"今天","focus":"default","focusing":"default","_focusing":"天气"}}]
     */
    private String raw_text;
    private String parsed_text;
    private List<ResultsEntity> results;

    public void setRaw_text(String raw_text) {
        this.raw_text = raw_text;
    }

    public void setParsed_text(String parsed_text) {
        this.parsed_text = parsed_text;
    }

    public void setResults(List<ResultsEntity> results) {
        this.results = results;
    }

    public String getRaw_text() {
        return raw_text;
    }

    public String getParsed_text() {
        return parsed_text;
    }

    public List<ResultsEntity> getResults() {
        return results;
    }

    public class ResultsEntity {
        /**
         * score : 1
         * domain : weather
         * update : 1
         * intent : get
         * demand : 0
         * object : {"date":"2015-09-10,2015-09-10","_date":"今天","focus":"default","focusing":"default","_focusing":"天气"}
         */
        private int score;
        private String domain;
        private int update;
        private String intent;
        private int demand;
        private ObjectEntity object;

        public void setScore(int score) {
            this.score = score;
        }

        public void setDomain(String domain) {
            this.domain = domain;
        }

        public void setUpdate(int update) {
            this.update = update;
        }

        public void setIntent(String intent) {
            this.intent = intent;
        }

        public void setDemand(int demand) {
            this.demand = demand;
        }

        public void setObject(ObjectEntity object) {
            this.object = object;
        }

        public int getScore() {
            return score;
        }

        public String getDomain() {
            return domain;
        }

        public int getUpdate() {
            return update;
        }

        public String getIntent() {
            return intent;
        }

        public int getDemand() {
            return demand;
        }

        public ObjectEntity getObject() {
            return object;
        }

        public class ObjectEntity {
            /**
             * date : 2015-09-10,2015-09-10
             * _date : 今天
             * focus : default
             * focusing : default
             * _focusing : 天气
             */
            private String date;
            private String _date;
            private String focus;
            private String focusing;
            private String _focusing;
            private String region;

            public String getRegion() {
				return region;
			}

			public void setRegion(String region) {
				this.region = region;
			}

			public void setDate(String date) {
                this.date = date;
            }

            public void set_date(String _date) {
                this._date = _date;
            }

            public void setFocus(String focus) {
                this.focus = focus;
            }

            public void setFocusing(String focusing) {
                this.focusing = focusing;
            }

            public void set_focusing(String _focusing) {
                this._focusing = _focusing;
            }

            public String getDate() {
                return date;
            }

            public String get_date() {
                return _date;
            }

            public String getFocus() {
                return focus;
            }

            public String getFocusing() {
                return focusing;
            }

            public String get_focusing() {
                return _focusing;
            }
        }
    }
}
