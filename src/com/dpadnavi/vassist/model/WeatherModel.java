package com.dpadnavi.vassist.model;

import java.util.ArrayList;

/**
 * @author dg
 * @desc 百度车联网天气的Bean 
 * @version 创建时间：2015-9-11
 */
public class WeatherModel extends BaseModel {

	public ArrayList<City> results;
	
	public static class City{
		public String currentCity;
		public String pm25;
		public ArrayList<WeatherData> weather_data;
	}
	
	public static class WeatherData{
		public String date;
		public String dayPictureUrl;
		public String nightPictureUrl;
		public String weather;
		public String wind;
		public String temperature;
	}
}
