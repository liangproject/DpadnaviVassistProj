package com.dpadnavi.vassist.model;

import java.io.Serializable;
import java.util.List;

public class SongModel implements Serializable{
	public static final String INTENT_SONGMODEL = "intent_songmodel";
	
	private String name;
	private String type;
    private List<String> byartist;
    private String album;
    private String genre;
    private List<String> tag;
    private String country;
    private String audience;
    private String mood;
    private String movie;
    private String tv;
    private String program;
    private String inlanguage;
    private String opera;
    private String instrument;
    
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<String> getByartist() {
		return byartist;
	}
	public void setByartist(List<String> byartist) {
		this.byartist = byartist;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public List<String> getTag() {
		return tag;
	}
	public void setTag(List<String> tag) {
		this.tag = tag;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAudience() {
		return audience;
	}
	public void setAudience(String audience) {
		this.audience = audience;
	}
	public String getMood() {
		return mood;
	}
	public void setMood(String mood) {
		this.mood = mood;
	}
	public String getMovie() {
		return movie;
	}
	public void setMovie(String movie) {
		this.movie = movie;
	}
	public String getTv() {
		return tv;
	}
	public void setTv(String tv) {
		this.tv = tv;
	}
	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getInlanguage() {
		return inlanguage;
	}
	public void setInlanguage(String inlanguage) {
		this.inlanguage = inlanguage;
	}
	public String getOpera() {
		return opera;
	}
	public void setOpera(String opera) {
		this.opera = opera;
	}
	public String getInstrument() {
		return instrument;
	}
	public void setInstrument(String instrument) {
		this.instrument = instrument;
	}
    
}
