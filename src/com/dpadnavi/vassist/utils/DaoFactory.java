package com.dpadnavi.vassist.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * 
 * @Module : 隶属模块名
 * @Comments : 描述
 * @Author : tomliang
 * @CreateDate : 2015-9-2
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-2
 * @Modified: 2015-9-2: 根据配置生成相应的操作类，可以配置切换模块
 */
public class DaoFactory {

	private static Properties properties;
	private static DaoFactory factory = new DaoFactory();
	
	static {
		properties = new Properties();
		try {
			properties.load(DaoFactory.class.getClassLoader().getResourceAsStream("dao.properties"));

		} catch (IOException e) {
			LogUtils.e("找不到dao.properties");
			e.printStackTrace();
		}
	}
	
	private DaoFactory(){}
	public static DaoFactory getInstance(){
		return factory;
	}
	@SuppressWarnings("unchecked")
	public <T> T getDao(Class<T> clazz){
		String name = clazz.getSimpleName();
		String className = properties.getProperty(name);
		try {
			return (T) Class.forName(className).newInstance();
		} catch (Exception e) {
			LogUtils.e("创建实例对象异常");
			e.printStackTrace();
		}
		return null;
	}
}
