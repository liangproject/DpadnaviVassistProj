package com.dpadnavi.vassist.utils;

import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.ListAdapter;
import android.widget.ListView;

public class ViewUtils {
	/** 把自身从父View中移除 */
	public static void removeSelfFromParent(View view) {
		if (view != null) {
			ViewParent parent = view.getParent();
			if (parent != null && parent instanceof ViewGroup) {
				ViewGroup group = (ViewGroup) parent;
				group.removeView(view);
			}
		}
	}

	/** 请求View树重新布局，用于解决中层View有布局状态而导致上层View状态断裂 */
	public static void requestLayoutParent(View view, boolean isAll) {
		ViewParent parent = view.getParent();
		while (parent != null && parent instanceof View) {
			if (!parent.isLayoutRequested()) {
				parent.requestLayout();
				if (!isAll) {
					break;
				}
			}
			parent = parent.getParent();
		}
	}

	/** 判断触点是否落在该View上 */
	public static boolean isTouchInView(MotionEvent ev, View v) {
		int[] vLoc = new int[2];
		v.getLocationOnScreen(vLoc);
		float motionX = ev.getRawX();
		float motionY = ev.getRawY();
		return motionX >= vLoc[0] && motionX <= (vLoc[0] + v.getWidth()) && motionY >= vLoc[1] && motionY <= (vLoc[1] + v.getHeight());
	}

	/** FindViewById的泛型封装，减少强转代码 */
	public static <T extends View> T findViewById(View layout, int id) {
		return (T) layout.findViewById(id);
	}
	
	
	 /**
	  * 设置listview的高度为item条目总高度
	 * @param listView
	 */
	public static void setListViewHeightBasedOnChildren(ListView listView) {  
		
	    ListAdapter listAdapter = listView.getAdapter();   
        if (listAdapter == null) {  
            // pre-condition  
            return;  
        }  
  
        int totalHeight = 0; 
        int count = listAdapter.getCount();
        totalHeight=count*UIUtils.dip2px(40);
//        for (int i = 0; i < listAdapter.getCount(); i++) {  
//            View listItem = listAdapter.getView(i, null, listView);  
//            listItem.measure(0, 0);  
//            //totalHeight += listItem.getMeasuredHeight();  
//        }  
  
        ViewGroup.LayoutParams params = listView.getLayoutParams();  
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));  
        listView.setLayoutParams(params);
		
		
		
//	        ListAdapter listAdapter = listView.getAdapter();   
//	        if (listAdapter == null) {  
//	            // pre-condition  
//	            return;  
//	        }  
//	        
//	        int makeMeasureSpec = MeasureSpec.makeMeasureSpec(UIUtils.dip2px(40), MeasureSpec.EXACTLY);
//	        int totalHeight = 0; 
//	        int count = listAdapter.getCount();
//	        for (int i = 0; i < listAdapter.getCount(); i++) {
//	            View listItem = listAdapter.getView(i, null, listView);
//	            listItem.measure(makeMeasureSpec, makeMeasureSpec);  
//	            totalHeight += listItem.getMeasuredHeight();  
//	        }  
//	  
//	        ViewGroup.LayoutParams params = listView.getLayoutParams();  
//	        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));  
//	        listView.setLayoutParams(params);  
	}  
	/**
	 * 设置listview的高度为item条目总高度
	 * @param listView
	 */
	public static void setListViewHeightBasedOnChildren2(ListView listView) {  
		
	        ListAdapter listAdapter = listView.getAdapter();   
	        if (listAdapter == null) {  
	            // pre-condition  
	            return;  
	        }  
	        
	        int totalHeight = 0;
	        for (int i = 0, len = listAdapter.getCount(); i < len; i++) 
	        {   
	            View listItem = listAdapter.getView(i, null, listView);
	            listItem.measure(0, 0);  
	            totalHeight += listItem.getMeasuredHeight();  //统计所有子项的总高度
	        }
	    
	        ViewGroup.LayoutParams params = listView.getLayoutParams();
	        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
	        listView.setLayoutParams(params);
	}  
}
