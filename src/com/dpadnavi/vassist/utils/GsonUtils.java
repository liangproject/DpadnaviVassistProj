package com.dpadnavi.vassist.utils;

import com.google.gson.Gson;

public class GsonUtils {

	
	public static <T> T json2bean(String result ,Class<T> clazz){
		Gson gson = new Gson();
		LogUtils.i("GsonUtils", result);
		T t = gson.fromJson(result, clazz);
		return t;
	}
	
	public static String bean2json(Object obj){
		Gson gson = new Gson();
		return gson.toJson(obj);
	}
	
}
