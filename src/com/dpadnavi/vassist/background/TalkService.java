/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : TalkService.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.background
 * @Author : tomliang
 * @CreateDate : 2015-9-3
 */
package com.dpadnavi.vassist.background;

import com.dpadnavi.vassist.interfaces.imp.BaiduVoiceProtocolImp;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * @Module : 隶属模块名
 * @Comments : 实现后台监听语音
 * @Author : tomliang
 * @CreateDate : 2015-9-3
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-3
 * @Modified: 2015-9-3: 实现基本功能
 */
public class TalkService extends Service {

	private BaiduVoiceProtocolImp mBaiduVoice;

	/* (non-Javadoc)
	 * @see android.app.Service#onBind(android.content.Intent)
	 */
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	/* (non-Javadoc)
	 * @see android.app.Service#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		mBaiduVoice = new BaiduVoiceProtocolImp();
		mBaiduVoice.init(getApplicationContext());
	}
	
	/* (non-Javadoc)
	 * @see android.app.Service#onStartCommand(android.content.Intent, int, int)
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		mBaiduVoice.start();
		return super.onStartCommand(intent, flags, startId);
	}
	
	/* (non-Javadoc)
	 * @see android.app.Service#onDestroy()
	 */
	@Override
	public void onDestroy() {
	 	super.onDestroy();
	 	mBaiduVoice.destory();
	}
}
