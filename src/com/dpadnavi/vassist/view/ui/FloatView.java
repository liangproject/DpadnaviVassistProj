package com.dpadnavi.vassist.view.ui;

import com.dpadnavi.vassist.utils.LogUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import android.widget.FrameLayout;

public class FloatView extends FrameLayout {
	private static final String TAG = "FloatView";
	protected WindowManager.LayoutParams mWindowParams = new WindowManager.LayoutParams();
	protected WindowManager mWindowManager;

	private boolean mShown = false;
	protected Point mWindowSize = new Point();
	@SuppressLint("NewApi") public FloatView(Context context) {
		super(context);
		mWindowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		Display display = mWindowManager.getDefaultDisplay();
		if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR2) {
			mWindowSize.y = display.getHeight();
			mWindowSize.x = display.getWidth();
		} else {
			display.getSize(mWindowSize);
		}
	}

	@Override
	public boolean isShown() {
		return mShown;
	}

	public void show() {
		if (mShown) {
			return;
		}
		LogUtils.i(TAG, "show");
		mShown = true;
		mWindowManager.addView(this, mWindowParams);
	}

	public void hide() {
		if (mShown) {
			LogUtils.i(TAG, "hide");
			mShown = false;
			mWindowManager.removeView(this);
		}
	}
}
