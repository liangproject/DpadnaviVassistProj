package com.dpadnavi.vassist.view.ui;

import com.dpadnavi.vassist.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
/*
 * 使用的主题样式
	<style name="foodDialog" parent="@android:Theme.Dialog">
	<item name="android:windowFrame">@null</item>
	<item name="android:windowNoTitle">true</item>
	<item name="android:windowContentOverlay">@null</item>
	<!-- 背景颜色及透明程度 -->
	<item name="android:windowBackground">@android:color/transparent</item>
	<!-- 是否浮现在activity之上 -->
	<item name="android:windowIsFloating">true</item>
	<!-- 是否模糊 -->
	<item name="android:backgroundDimEnabled">false</item>
	</style>
*/
public class CustomLoadingDialog extends Dialog {
	private Context mContext = null;
	private TextView mMessage=null;
	private ImageView mLoadingImage;
	
	private static CustomLoadingDialog customLoadingDialog = null;
	
	public CustomLoadingDialog(Context context){
		super(context);
		this.mContext=context;
	}
	
	public CustomLoadingDialog(Context context, int theme) {
        super(context, theme);
        this.mContext=context;
    }
	
	/**
	 * 获取Loading示例对象
	 * @param context
	 * @return
	 */
	public static CustomLoadingDialog getInstance(final Context context){
		
		if(null==customLoadingDialog || !context.equals(customLoadingDialog.mContext)){
			customLoadingDialog = new CustomLoadingDialog(context,R.style.loadingDialog);
			View view = View.inflate(context, R.layout.lib_loading_process, null);
			customLoadingDialog.setContentView(view);
			Window window = customLoadingDialog.getWindow();
			window.setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		}
		
		//customProgressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
		//从dialog获取window
//		Window window = customLoadingDialog.getWindow();
		//设置重心显示位置
//		window.setGravity(Gravity.BOTTOM);
//		//设置动画，为自定的样式
//		window.setWindowAnimations(R.style.dialog_anim_style);
//		//设置这个window的Layout属性
//		window.setLayout(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);
		
		//设置为False点击dialog以为的部分也不会关闭dialog
		//customProgressDialog.setCanceledOnTouchOutside(false);
		
		((TextView)customLoadingDialog.findViewById(R.id.id_tv_loadingmsg)).setVisibility(View.GONE);
		
		return customLoadingDialog;
	}
	
	@Override
    public void onWindowFocusChanged(boolean hasFocus){
    	if (customLoadingDialog == null){
    		return;
    	}
    	
    	if(null==mLoadingImage)
    		mLoadingImage = (ImageView) customLoadingDialog.findViewById(R.id.loadingImageView);
    	
//        AnimationDrawable animationDrawable = (AnimationDrawable) mLoadingImage.getBackground();
//        animationDrawable.start();
        Animation rotateAnim = AnimationUtils.loadAnimation(mContext, R.anim.loading_rotate);
        mLoadingImage.setAnimation(rotateAnim);
    }
 
	 /**
	  * 显示默认的loading提示内容（正在加载……）
	 * @return
	 */
	public CustomLoadingDialog showMessage(){
		if(null==mMessage)
    		mMessage = (TextView)customLoadingDialog.findViewById(R.id.id_tv_loadingmsg);
		if (mMessage != null){
    		mMessage.setVisibility(View.VISIBLE);
    	}
		 return customLoadingDialog;
	 }
	 
    /**
     * 显示指定loading下的提示内容
     * @param strMessage
     * @return
     */
    public CustomLoadingDialog showMessage(String strMessage){
    	if(null==mMessage)
    		mMessage = (TextView)customLoadingDialog.findViewById(R.id.id_tv_loadingmsg);
    	if (mMessage != null){
    		mMessage.setText(strMessage);
    		mMessage.setVisibility(View.VISIBLE);
    	}
    	return customLoadingDialog;
    }
}