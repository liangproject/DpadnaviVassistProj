package com.dpadnavi.vassist.view.activitis;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.dpadnavi.vassist.R;
import com.dpadnavi.vassist.background.TalkService;
import com.dpadnavi.vassist.interfaces.imp.BaiduVoiceProtocolImp;

public class MainActivity extends BaseActivity {

	private Button btn;

	/* (non-Javadoc)
	 * @see com.dpadnavi.vassist.view.activitis.BaseActivity#setLayout()
	 */
	@Override
	void setLayout() {
		setContentView(R.layout.activity_main);
	}

	/* (non-Javadoc)
	 * @see com.dpadnavi.vassist.view.activitis.BaseActivity#initView()
	 */
	@Override
	void initView() {
		btn = (Button) findViewById(R.id.btn);
		
		btn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
//				mBaiduVoiceProtocolImp.start();
				Intent talkService = new Intent(MainActivity.this, TalkService.class);
				startService(talkService);
			}
		});
	}
    
}
