/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : BaseActivity.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.vassist.view.activitis
 * @Author : tomliang
 * @CreateDate : 2015-9-2
 */
package com.dpadnavi.vassist.view.activitis;

import com.dpadnavi.vassist.manager.ActivityManager;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * @Module : 隶属模块名
 * @Comments : Activity父类
 * @Author : tomliang
 * @CreateDate : 2015-9-2
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-2
 * @Modified: 2015-9-2: 实现基本功能
 */
public abstract class BaseActivity extends FragmentActivity {
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		ActivityManager.getManager().pushActivity(this);
		getDataFromBefore();
		getSavedInstanceState(bundle);
		setLayout();
		initView();
		setEventListener();
		initData();
		loadData();
		setData();
	}

	/** 
	 * @author : tomliang
	 * @description : 获取传递进来的数据
	 * @date : 2015-9-2 : void 
	 */
	private void getDataFromBefore() {
		
	}

	/** 
	 * @author : tomliang
	 * @description : 获取保存的数据
	 * @date : 2015-9-2
	 * @param bundle : void 
	 */
	private void getSavedInstanceState(Bundle bundle) {
		
	}
	
	/** 
	 * @author : tomliang
	 * @description : 设置布局
	 * @date : 2015-9-2 : void 
	 */
	abstract void setLayout();

	/** 
	 * @author : tomliang
	 * @description : 初始化控件
	 * @date : 2015-9-2 : void 
	 */
	abstract void initView();

	/** 
	 * @author : tomliang
	 * @description : 设置监听
	 * @date : 2015-9-2 : void 
	 */
	private void setEventListener() {
		
	}

	/** 
	 * @author : tomliang
	 * @description : 初始化数据
	 * @date : 2015-9-2 : void 
	 */
	private void initData() {
		
	}

	/** 
	 * @author : tomliang
	 * @description : 拉取数据
	 * @date : 2015-9-2 : void 
	 */
	private void loadData() {
		
	}

	/** 
	 * @author : tomliang
	 * @description : 设置数据
	 * @date : 2015-9-2 : void 
	 */
	private void setData() {
		
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onDestroy()
	 */
	@Override
	protected void onDestroy() {
		super.onDestroy();
		ActivityManager.getManager().pushActivity(this);
	}
}
