/**
 * Copyright (c) 2012-2014 Yunzhisheng(Shanghai) Co.Ltd. All right reserved.
 * @FileName : MyTest.java
 * @ProjectName : DpadnaviVassistProj
 * @PakageName : com.dpadnavi.test
 * @Author : tomliang
 * @CreateDate : 2015-9-8
 */
package com.dpadnavi.test;

import org.json.JSONException;
import org.json.JSONObject;
import com.dpadnavi.vassist.model.BaiduVoiceResponeModel;
import com.dpadnavi.vassist.utils.GsonUtils;
import android.test.AndroidTestCase;

/**
 * @Module : 单元测试模块
 * @Comments : 进行单元测试
 * @Author : tomliang
 * @CreateDate : 2015-9-8
 * @ModifiedBy : 修改人
 * @ModifiedDate: 2015-9-8
 * @Modified: 2015-9-8: 实现基本功能
 */
public class MyTest extends AndroidTestCase {

	public void test_paseJson(){
		String json = "{\"content\":{\"item\":[\"北京明天天气怎么样\",\"北京明天天气怎麽样\",\"北京明天天气怎么样\",\"北京明天天气怎么样的\"],\"json_res\":\"{\\\"parsed_text\\\":\\\"北京 明天 天气 怎么样\\\",\\\"raw_text\\\":\\\"北京明天天气怎么样\\\",\\\"results\\\":[{\\\"demand\\\":0,\\\"domain\\\":\\\"weather\\\",\\\"intent\\\":\\\"get\\\",\\\"object\\\":{\\\"_date\\\":\\\"明天\\\",\\\"_focusing\\\":\\\"天气\\\",\\\"date\\\":\\\"2015-09-09,2015-09-09\\\",\\\"focus\\\":\\\"default\\\",\\\"focusing\\\":\\\"default\\\",\\\"region\\\":\\\"北京\\\"},\\\"score\\\":1,\\\"update\\\":1}]}\\n\"},\"result\":{\"corpus_no\":6191811472333118000,\"err_no\":0,\"idx\":27,\"res_type\":3,\"sn\":\"ee2504a4-980f-4333-95c6-4fa42ed61ef1\"}}";
		try {
			JSONObject obj = new JSONObject(json);
			String content = obj.getString("content");
			if(content != null || !"".equals(content)){
				JSONObject contentobj = new JSONObject(content);
				String json_res = contentobj.getString("json_res");
				BaiduVoiceResponeModel model = GsonUtils.json2bean(json_res, BaiduVoiceResponeModel.class);
				if(model != null){
					System.out.println(model.getResults().get(0).getDomain());
				}
			}
			System.out.println();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public void test_paser(){
		String json = "{\"parsed_text\":\"北京 明天 天气 怎么样\",\"raw_text\":\"北京明天天气怎么样\",\"results\":[{\"demand\":0,\"domain\":\"weather\",\"intent\":\"get\",\"object\":{\"_date\":\"明天\",\"_focusing\":\"天气\",\"date\":\"2015-09-09,2015-09-09\",\"focus\":\"default\",\"focusing\":\"default\",\"region\":\"北京\"},\"score\":1,\"update\":1}]}";
		BaiduVoiceResponeModel model = GsonUtils.json2bean(json, BaiduVoiceResponeModel.class);
		System.out.println(model.getParsed_text());
	}
}
